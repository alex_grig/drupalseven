<?php


/**
 * Implements security_settings_form().
 */
function pizza_add_pizzatype_settings_form($form, &$form_state) {  

$form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('title'),
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['pizzatype'] = array(
    '#type' => 'textfield',
    '#title' => t('pizza'),
    '#required' => TRUE,
    '#maxlength' => 255,
    '#weight' => -4,
  );

  $form['price_pizza'] = array(
    '#type' => 'textfield',
    '#title' => t('price'),
    '#required' => TRUE,
    '#maxlength' => 255,
    '#weight' => -4,
  );

  $form['available_pizza'] = array(
    '#type' => 'checkbox',
    '#title' => t('available'),
    '#maxlength' => 255,
    '#weight' => -4,
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' =>  'Save',
    '#submit' =>  array('pizza_add_pizzatype_settings_form_submit'),
);

  return $form;
}


/**
 * Implements security_settings_form().
 */
function pizza_add_area_settings_form($form, &$form_state) {  

     $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('title'),
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['area'] = array(
    '#type' => 'textfield',
    '#title' => t('area'),
    '#required' => TRUE,
    '#maxlength' => 255,
    '#weight' => -4,
  );

  $form['price_area'] = array(
    '#type' => 'textfield',
    '#title' => t('price'),
    '#required' => TRUE,
    '#maxlength' => 255,
    '#weight' => -4,
  );

  $form['available_area'] = array(
    '#type' => 'checkbox',
    '#title' => t('available'),
    
    '#maxlength' => 255,
    '#weight' => -4,
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' =>  'Save',
    '#submit' =>  array('pizza_add_area_settings_form_submit'),
);
    
    return $form;
}


/**
 * Implements security_settings_form_submit().
 */
function pizza_add_area_settings_form_submit($form, $form_state) {
  global $user; 
  db_insert('areatype')
    ->fields(array(
      'area' => $form_state['values']['area'],
      'price' => $form_state['values']['price_area'],   
      'available' => $form_state['values']['available_area'],          
    ))->execute();
    drupal_set_message("successfully saved"); 
}

function pizza_add_pizzatype_settings_form_submit($form, $form_state) {
    db_insert('pizzatype')
    ->fields(array(
      'pizza' => $form_state['values']['pizzatype'],
      'price' => $form_state['values']['price_pizza'],   
      'available' => $form_state['values']['available_pizza'],          
    ))->execute();
    drupal_set_message("successfully saved"); 
}

  
