<?php

function form_api_form($form, &$form_state) {

$form['name'] = [
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Name.'),
    '#required' => TRUE,
  ];

$form['email'] = [
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#description' => t('Email.'),
    '#required' => TRUE,
];

$year_birth = range(1900,2010);

$form['year_birth'] = [
    '#type' => 'select',
    '#title' => t('Year of birth'),
    '#options' => $year_birth,
];

$form['gender'] = [
    '#type' => 'radios',
    '#title' => t('Gender'),
    '#options' => [
      '0' => t('Male'),
      '1' => t('Female'),
    ],
    '#required' => TRUE,
    '#default_value' => 0,
];

$form['limbs'] = [
    '#type' => 'radios',
    '#title' => t('Number limbs'),
    '#options' => [
      '0' => ('0'),
      '1' => ('1'),
      '2' => ('2'),
      '3' => t('More'),
    ],
    '#required' => TRUE,
    '#default_value' => 0,
];

$form['superpower'] = [
    '#type' => 'select',
    '#title' => t('Superpower'),
    '#options' => [
      '0' => t('Immortality'),
      '1' => t('Passing through walls'),
      '2' => t('Levitation'),
    ],
    '#multiple' => TRUE,
    '#required' => TRUE,
];

$form['biography'] = [
    '#type' => 'textarea',
    '#title' => t('Biography'),
    '#required' => TRUE,
];

$form['check'] = [
    '#title' => t('I have read the contract'),
    '#type' => 'checkbox',
    '#required' => TRUE,
];

$form['actions'] = [
    '#type' => 'actions',
];

$form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Send'),
];

return $form;
}


function form_api_form_submit($form, &$form_state) {

$super_option = array();

foreach ($form_state['values']['superpower'] as $key => $super) {
  $super_option [] = $form['superpower']['#options'][$super];
}

drupal_set_message(t('Form has been sent'));

$module = 'form_api';
$key = 'form_api';
$to = variable_get('site_mail', '');

global $language;

$params = array(

  'name' => $form_state['values']['name'],
  'email' => $form_state['values']['email'],
  'year_birth' => $form['year_birth']['#options'][$form_state['values']['year_birth']],
  'gender' => $form['gender']['#options'][$form_state['values']['gender']],
  'limbs' => $form_state['values']['limbs'],
  'superpower' => implode(', ', $super_option),
  'biography' => $form_state['values']['biography']
  
);

drupal_mail($module, $key, $to, $language, $params, $from = NULL, $send = TRUE);

watchdog('form_api', 'Message  = %params' , array('%params' => implode(',', $params)));
}
