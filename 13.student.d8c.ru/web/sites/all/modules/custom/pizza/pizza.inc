<?php


function pizza_order_service($form, &$form_state) {

  $pizzas = array();

  $query = db_select('pizzatype', 'n')
      ->fields('n', array('nid', 'pizza', 'price', 'available'))
      ->execute();

  foreach ($query as $node){

    $nid = $node->nid;

    $pizza = $node->pizza;
    $price = $node->price;
    $available = $node->available;

    $pizzas[$nid]["pizza"] = $pizza;
    $pizzas[$nid]["price"] = $price;
    $pizzas[$nid]["available"] = $available;

  }

  $areas = array();

  $query = db_select('areatype', 'n')
      ->fields('n', array('nid', 'area', 'price', 'available'))
      ->execute();

  foreach ($query as $node){

    $nid = $node->nid;

    $area = $node->area;
    $price = $node->price;
    $available = $node->available;

    $areas[$nid]["area"] = $area;
    $areas[$nid]["price"] = $price;
    $areas[$nid]["available"] = $available;

  }

  foreach ($pizzas as $key => $name) {
    if (!$name['available']) {
      unset($pizzas[$key]);
    }
  }

    $form['quantity'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
  ];

  foreach ($pizzas as $key => $name) {
    $form['quantity'][$key] = [
      '#type' => 'select',
      '#options' => range(0, 10),
      '#prefix' => $name["pizza"] . '. Цена за штуку: ' . $name["price"],
  ];
}

  foreach ($areas as $key => $name) {
    if ($name['available'] === 0) {
      unset($areas[$key]);
    }
  }
    
  foreach ($areas as $key => $name) {
    $areas[$key]['area'] .= '. Цена доставки: ' . $areas[$key]['price'];
  }

  $form['area'] = [
    '#type' => 'radios',
    '#options' => array_combine(array_keys($areas), array_column($areas, 'area')),
    '#required' => TRUE,
    '#title' => 'Ваш район',
  ];

  $form['phone'] = [
    '#type' => 'tel',
    '#title' => 'Телефон',
    '#required' => TRUE,
  ];

  $form['address'] = [
    '#type' => 'textfield',
    '#title' => 'Адрес',
    '#required' => TRUE,
  ];

  $form['price'] = [
    '#type' => 'textfield',
    '#title' => t('Сумма к оплате'),
    '#disabled' => TRUE,
    '#id' => 'price',
];

  $form['submit'] = [
    "#type" => "submit",
    '#value' => t('Отправить'),
  ];

  drupal_add_js(drupal_get_path('module', 'pizza') . '/pizza.js');

  $settings = array ( 'pizzas' => $pizzas, 'areas' => $areas, );
  drupal_add_js( array ("pizza" => $settings), 'setting');

  return $form;

}

function pizza_order_service_validate($form, &$form_state) {

  if (isset($form_state['values']['phone'])) {
    $phone = $form_state['values']['phone'];

    if (!preg_match('^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$^', $phone)) {
      form_set_error('phone', t('Phone write ERROR'));
    }
  }

}

function pizza_submit($form, &$form_state) {
  
   $module = 'pizza';
   $key = 'pizza';
   $to = variable_get('site_mail', '');
   $language = 'ru';
  
   $count = 0;
   $message_order_pizzas = null;
   foreach ($pizzas as $key => $name) {
    if ($form_state['values']["quantity"][$key] > 0) {
   $count = $form_state['values']["quantity"][$key];
   $message_order_pizzas = $message_order_pizzas . $name["pizza"]. " в количестве " .$count . " штук за " . $name["price"] . "₽, ";
    }
   }
  
   $params = array(
    'address' => " по адрессу - " . $form_state['values']['address'],
    'quantity' => " " . $message_order_pizzas,
    'area' => $form['area']['#options'][$form_state['values']['area']],
    
   );
  
   drupal_mail($module, $key, $to, $language, $params, $from = NULL, $send = TRUE);
   drupal_set_message("Спасибо за заказ! Ожидайте");
   watchdog('forma', '%params' , array('%params' => implode(',', $params)));
  }



  



