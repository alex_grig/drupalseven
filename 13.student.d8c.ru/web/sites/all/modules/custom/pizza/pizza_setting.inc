<?php


function pizza_order_settings($form, &$form_state){

$pizzas = array();
$types_price = array();

$query = db_select('pizzatype', 'n')
    ->fields('n', array('nid', 'pizza', 'price', 'available'))
    ->execute();

foreach ($query as $node){

  $nid = $node->nid;

  $pizza = $node->pizza;
  $price = $node->price;
  $available = $node->available;

  $pizzas[$nid]["pizza"] = $pizza;
  $pizzas[$nid]["price"] = $price;
  $pizzas[$nid]["available"] = $available;

  $pizzas_price[$nid] = $price;

}

$areas = array();
$areas_price = array();

$query = db_select('areatype', 'n')
    ->fields('n', array('nid', 'area', 'price', 'available'))
    ->execute();

foreach ($query as $node){

  $nid = $node->nid;

  $area = $node->area;
  $price = $node->price;
  $available = $node->available;

  $areas[$nid]["area"] = $area;
  $areas[$nid]["price"] = $price;
  $areas[$nid]["available"] = $available;

  $areas_price[$nid] = $price;

}

$form['add_area'] = array(
  '#type' => 'submit',
  '#value' =>  'add area',
  '#submit' => array('_redirect_form_area'),
);

$form['add_pizza'] = array(
  '#type' => 'submit',
  '#value' =>  'add pizza',
  '#submit' => array('_redirect_form_pizza'),
);


  $form['price'] = [
    '#type' => 'fieldset',
    '#title' => t('Виды пицц и их цены'),
    '#tree' => TRUE,
];

foreach ($pizzas as $key => $name) {
  $form['price'][$key] = [
    '#type' => 'textfield',
    '#default_value' => $name["price"],
    '#prefix' => $name["pizza"],
];
}
  
$form['price_area'] = [
  '#type' => 'fieldset',
  '#title' => t('Цена доставки в район'),
  '#tree' => TRUE,
];


foreach ($areas as $key => $name) {
  $form['price_area'][$key] = [
    '#type' => 'textfield',
    '#default_value' => $name["price"],
    '#prefix' => $name["area"],
];
}

$form['available_pizza'] = [
  '#type' => 'fieldset',
  '#title' => t('Доступные пиццы'),
  '#tree' => TRUE,
];

foreach ($pizzas as $key => $name) {
  $form['available_pizza'][$key] = [
    '#type' => 'checkbox',
    '#default_value' => $name["available"],
    '#prefix' => $name["pizza"],
];
}

$form['available_area'] = [
  '#type' => 'fieldset',
  '#title' => t('Доступные районы'),
  '#tree' => TRUE,
];

foreach ($areas as $key => $name) {
  $form['available_area'][$key] = [
    '#type' => 'checkbox',
    '#default_value' => $name["available"],
    '#prefix' => $name["area"],
];
}

$form['delete_area'] = [
  '#type' => 'fieldset',
  '#title' => t('Удалить район'),
  '#tree' => TRUE,
];

foreach ($areas as $key => $name) {
  $form['delete_area'][$key] = [
    '#type' => 'checkbox',
   
    '#prefix' => $name["area"],
];
}

$form['delete_pizza'] = [
  '#type' => 'fieldset',
  '#title' => t('Удалить пиццу'),
  '#tree' => TRUE,
];

foreach ($pizzas as $key => $name) {
  $form['delete_pizza'][$key] = [
    '#type' => 'checkbox',
    
    '#prefix' => $name["pizza"],
];
}

$form['submit'] = [
  "#type" => "submit",
  '#value' => t('Изменить'),
];

return $form;

}

function pizza_order_settings_submit($form, &$form_state) {
  variable_set('setprice', $form_state['values']['price']);
  variable_set('setprice_area', $form_state['values']['price_area']);
  variable_set('setavailable_pizza', $form_state['values']['available_pizza']);
  variable_set('setavailable_area', $form_state['values']['available_area']);

  $get_pizza = variable_get('setprice');
  $get_area = variable_get('setprice_area');
  $get_available_pizza = variable_get('setavailable_pizza');
  $get_available_area = variable_get('setavailable_area');

  foreach ($get_pizza as $i => $fee) {
    db_update('pizzatype')
      ->condition('nid', $i)
      ->fields(array(
        'price' => $fee ,
      ))
      ->execute();
}

  foreach ($get_area as $i => $fee) {
    db_update('areatype')
      ->condition('nid', $i)
      ->fields(array(
        'price' => $fee ,
      ))
      ->execute();
}

  foreach ($get_available_pizza as $i => $fee) {
    db_update('pizzatype')
      ->condition('nid', $i)
      ->fields(array(
        'available' => $fee ,
      ))
      ->execute();
}

  foreach ($get_available_area as $i => $fee) {
    db_update('areatype')
      ->condition('nid', $i)
      ->fields(array(
        'available' => $fee ,
      ))
      ->execute();
}

  variable_set('set_delete_area', $form_state['values']['delete_area']);
  $get_delete_area = variable_get('set_delete_area');

  foreach ($get_delete_area as $i => $fee) {
    if($fee == 1){

      db_delete('areatype')
        ->condition('nid', $i)
        ->execute();
    drupal_set_message("successfully remove"); 
   }
  }

  variable_set('set_delete_pizza', $form_state['values']['delete_pizza']);
  $get_delete_pizza = variable_get('set_delete_pizza');

  foreach ($get_delete_pizza as $i => $fee) {
    if($fee == 1){

      db_delete('pizzatype')
        ->condition('nid', $i)
        ->execute();
    drupal_set_message("successfully remove"); 
   }
  }
}

function _redirect_form_area($form, &$form_state) {
  drupal_goto('http://13.student.d8c.ru/pizza/settings/add_area');
}

function _redirect_form_pizza($form, &$form_state) {
  drupal_goto('http://13.student.d8c.ru/pizza/settings/add_pizzatype');
}

