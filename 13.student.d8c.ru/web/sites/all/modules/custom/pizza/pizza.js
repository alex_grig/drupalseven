(function ($, Drupal) {
    Drupal.behaviors.pizzaBehaviour = {
        attach: function (context, settings) {

            var pizza_price = 0;
            var area_price = 0;
            var selects = {};
            
            $('.form-select', context).change(function () {
                selects[$(this).attr('id')] = $(this).children('option:selected').val();
                pizza_price = 0;
                for (var key in selects) {
                    count = key.replace("edit-quantity-", "");
                    var price = Drupal.settings['pizza']['pizzas'][count]['price'];
                    pizza_price += price*selects[key];
                }
                $("#price").val(Number(Number(pizza_price) + Number(area_price)));
            });
            $('.form-radio', context).change(function () {
                count = $(this).attr('value');
                area_price = Drupal.settings['pizza']['areas'][count]['price'];
                $("#price").val(Number(pizza_price) + Number(area_price));
            });
        }
    }
})(jQuery, Drupal);
