<?php


function write_log_message_form($form, &$form_state) {
    
  $form['message'] = array(
    '#title' => t('Message'),
    '#type' => 'textarea',
    '#required' => TRUE,
  );

  if (user_is_anonymous()) {

    $form['name'] = array(
      '#title' => t('Name'),
      '#type' => 'textfield',
    );

    if (variable_get('set_contact_information') == 1) {

      $form['phone'] = array(
        '#title' => t('Phone'),
        '#type' => 'textfield',
        '#description' => t('+7..'),
      );

      $form['email'] = array(
        '#title' => t('Email'),
        '#type' => 'textfield',
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Send',
  );

  return $form;

}


function write_log_settings_form($form, &$form_state) {
  $form['contact_information'] = array(
    '#title' => t('Contact information'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('set_contact_information', 0),
  );

  $form['price'] = [
    '#type' => 'textfield',
    '#title' => t('Price'),
    '#disabled' => TRUE,
    '#id' => 'price',
];

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );  

  return $form;
}


function write_log_settings_form_submit($form, &$form_state) {
  variable_set('set_contact_information', $form_state['values']['contact_information']);
}


/**
 * Implements hook_form().
 */
function write_log_message_form_validate($form, &$form_state) {

  if (isset($form_state['values']['phone'])) {

    $number = $form_state['values']['phone'];

    if (strlen($number) > 0 && !preg_match('^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$^', $number)) {
      form_set_error('values][phone', t('Phone number must be in format xxx-xxx-nnnn-nnnn.'));
    }
  }

  if (isset($form_state['values']['email'])) {

    $email = $form_state['values']['email'];

    if (!valid_email_address($email)) {
      form_set_error('email', t('Incorrect form email'));
    }
  }

}


function write_log_message_form_submit($form, &$form_state) {

  $values = $form_state['values'];
  drupal_set_message(t('Sent)'));
  
  if (isset($values['name'])) {
    watchdog('write_log','message = %message , name = %name',array('%message' => $values['message'], '%name' => $values['name'] ));
  }
  else {
    global $user;
    watchdog('write_log','message = %message , name = %name',array('%message' => $values['message'], '%name' => $user->name ));
  }
  drupal_goto();
}
